pipeline {
    agent {
        docker { image 'python:3.8.10' }
    }
    // ENVIRONMENT VARIABLES
    environment {
        BACKEND_IMAGE_TAG="backend:latest"
        REGISTRY_IMAGE="registry.gitlab.com/gbazack/jenkins-project"
        REGISTRY_USER="gbazack"
        REGISTRY_TOKEN="glpat-zqwTcEci6-DyBCFFHA72"
        REGISTRY_URL="https://registry.gitlab.com"
    }// END OF ENVIRONMENT VARIABLES
    /****/
    // BEGIN STAGES
    stages {
        // STAGE: Gitlab repository checkout
        stage('checkout-gitlab') {
            steps {
                echo 'Checking out gitlab repository'
                checkout([$class: 'GitSCM', branches: [[name: '*/main']], browser: [$class: 'GitLabBrowser', projectUrl: 'https://gitlab.com/gbazack/jenkins-project'], extensions: [], userRemoteConfigs: [[url: 'https://gitlab.com/gbazack/jenkins-project']]])
            }
        }// END OF CHECKOU-GITLAB STAGE
        //
        // STAGE: Preparing the environment
        stage('Init-env') {
            steps {
                echo 'Preparing the environment'
                //sh 'make set-test-env'
            }
        }// END OF INIT-ENV STAGE
        //
        // STAGE: Security
        stage('Security') {
            parallel {
                // STAGE: Bandit test
                stage('Bandit-test') {
                    steps {
                        echo "Scanning common security issues in Python code"
                        sh '. venv/bin/activate'
                        sh 'bandit -r app/ | tee -a reports/bandit.json'
                        //sh 'make bandit-test'
                    }
                    post{
                        failure{ // OPTIONAL: USER MAY REMOVE THIS BLOCK
                            catchError(message: 'TEST FAILED', stageResult: 'FAILURE') {
                                echo "Stage failed, but pipeline continues"
                                sh 'exit 0'
                            }
                        }
                    }
                }// END OF BANDIT-TEST STAGE
                //
                // STAGE: Safety-check
                stage('Safety-check') {
                    steps {
                        echo "Check installed dependencies for known vulnerabilities"
                        sh '. venv/bin/activate'
                        sh 'safety check --full-report --json | tee -a reports/safety.json'
                        //sh 'make safety-check'
                    }
                    post{
                        failure{ // OPTIONAL: USER MAY REMOVE THIS BLOCK
                            catchError(message: 'TEST FAILED', stageResult: 'FAILURE') {
                                echo "Stage failed, but pipeline continues"
                                sh 'exit 0'
                            }
                        }
                    }
                }// END OF SAFETY-CHECK STAGE
            }// END OF PARALLEL
        }// END OF SECURITY STAGE
        //
        // STAGE: Code quality
        stage('Codequality') {
            parallel{
                // STAGE: Docker file lint with hadolint
                stage('Hadolint'){
                    agent {
                        docker { image 'hadolint/hadolint:v2.0.0-alpine' }
                    }
                    steps{
                        echo "Lint the dockerfile and docker-compose files"
                        writeFile file: 'reports/hadolint.json', text: ''
                        sh 'hadolint -f json app/Dockerfile | tee -a reports/hadolint.json'
                        //sh 'make hadolint'
                    }
                    post{
                        failure{// OPTIONAL: USER MAY REMOVE THIS BLOCK
                            catchError(message: 'HADOLINT - LINTING FAILED', stageResult: 'FAILURE'){
                                echo "Stage failed, but pipeline continues"
                                sh 'exit 0'
                            }
                        }
                        always{
                            recordIssues aggregatingResults: true, enabledForFailure: true, healthy: 1, minimumSeverity: 'HIGH', tools: [dockerLint(pattern: 'reports/hadolint.json'), hadoLint(pattern: 'reports/hadolint.json')], unhealthy: 9
                        }
                    }
                }// END OF HADOLINT STAGE
                //
                // STAGE: Python Flake8
                stage('Flake8'){
                    steps{
                        echo "Static testing with Python Flake8 package"
                        sh '. venv/bin/activate'
                        writeFile file: 'reports/flake8.json', text: ''
                        //sh 'python -m pip install flake8 pytest pytest-cov coverage'
                        sh 'flake8 --exit-zero --statistics app/ | tee -a reports/flake8.json'
                        //sh 'coverage run --source=app -m flake8 --exit-zero --ignore=E121,E122,E126,E127,E128,E241,E251,E265,F401,F403,F405,W391 --max-line-length 150 app'
                        //sh 'make flake'
                        //sh 'pytest --cov-report xml:coverage.xml --cov=app/'
                    }
                    post{
                        failure{// OPTIONAL: USER MAY REMOVE THIS BLOCK
                            catchError(message: 'FLAKE8 - CODE QUALITY TEST FAILED', stageResult: 'FAILURE'){
                                echo "Stage failed, but pipeline continues"
                                sh 'exit 0'
                            }
                        }
                        always {
                            recordIssues aggregatingResults: true, enabledForFailure: false, healthy: 1, minimumSeverity: 'HIGH', tools: [flake8(pattern: 'reports/flake8.json')], unhealthy: 9
                        }
                    }
                }// END OF PYTHON-FLAKE8 STAGE
                //
                // STAGE: Pylama
                stage('Pylama'){
                    steps{
                        echo "Python code audit with pylama"
                        sh '. venv/bin/activate'
                        writeFile file: 'reports/pylama.json', text: ''
                        //sh 'python -m pip install pylama[all] coverage pytest pytest-cov'
                        //sh 'coverage run -m pylama --ignore=E121,E122,E126,E127,E128,E241,E251,E265,F401,F403,F405,W391 --max-line-length 150 --max-complexity 9 app'
                        sh 'pylama app --format json | tee -a reports/pylama.json'
                        //sh 'pytest --cov-report xml:coverage-2.xml --cov=app/'
                        //sh 'make pylama'
                    }
                    post{
                        failure{// OPTIONAL: USER MAY REMOVE THIS BLOCK
                            catchError(message: 'CODE AUDIT FAILED', stageResult: 'FAILURE'){
                                echo "Stage failed, but pipeline continues"
                                sh 'exit 0'
                            }
                        }
                        //always{
                        //    recordIssues aggregatingResults: true, enabledForFailure: false, healthy: 1, minimumSeverity: 'HIGH', tools: [pep8(pattern: 'reports/pylama.json'), pyDocStyle(pattern: 'reports/pylama.json')], unhealthy: 9
                        //    sh 'exit 0'
                        //}
                    }
                }
            }// END OF PARALLEL
        }// END OF CODE QUALITY STAGE
        /****/
        // STAGE: Code coverage
        stage('Coverage'){
            steps{
                echo "Publishing Code coverage"
                sh '. venv/bin/activate'
                sh 'python -m pip install coverage'
                sh 'coverage report --no-skip-covered -m app/*.py app/vapormap/*.py app/vapormap/settings/*.py app/api/*.py app/api/migrations/*.py' //--format json --output-file reports/flake8.json'
                sh 'coverage html --no-skip-covered --show-contexts --ignore-errors app/*.py app/vapormap/*.py app/vapormap/settings/*.py app/api/*.py app/api/migrations/*.py'
                sh 'coverage xml --ignore-errors app/*.py app/vapormap/*.py app/vapormap/settings/*.py app/api/*.py app/api/migrations/*.py'
                //sh 'make coverage'
            }
            post{
                failure{// OPTIONAL: USER MAY REMOVE THIS BLOCK
                    catchError(message: 'COVERAGE - FAILED', stageResult: 'FAILURE'){
                        echo "Stage failed, but pipeline continues"
                        sh 'exit 0'
                    }
                }
                always {
                    publishHTML([allowMissing: false, alwaysLinkToLastBuild: true, includes: '**/*.html,**/*.css,**/*.js,**/*.png,**/*.jpg', keepAll: false, reportDir: 'htmlcov', reportFiles: 'index.html', reportName: 'Code Coverage Report', reportTitles: 'Python Code Coverage Report'])
                    //publishCoverage adapters: [cobertura('coverage.xml')], sourceFileResolver: sourceFiles('NEVER_STORE')
                }
            }
        }// END OF COVERAGE STAGE
        //
        // STAGE: Building Docker image
        stage('Build-image') {
            agent {
                docker { image 'docker:20.10' }
            }
            steps {
                echo "Building the docker image"
                script {
                    withDockerRegistry(url: '${REGISTRY_URL}') {
                        // LOGIN TO THE GITLAB REPOSITORY
                        sh 'docker login -u "${REGISTRY_USER}" -p "${REGISTRY_TOKEN}" "${REGISTRY_URL}"'
                        def Image = docker.build("${REGISTRY_IMAGE}/${BACKEND_IMAGE_TAG}", "./app")
                        Image.push()
                    }
                }
            }
            post{
                failure{// OPTIONAL: USER MAY REMOVE THIS BLOCK
                    catchError(message: 'DOCKER IMAGE - BUILD  FAILED', stageResult: 'FAILURE') {
                        echo "Stage failed, but pipeline continues"
                        sh 'exit 0'
                    }
                }
            }
        } //END OF BUILD-IMAGE STAGE
        //
        //
    }// END OF STAGES    
}// END OF PIPELINE
