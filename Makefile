all: set-test-env

set-test-env:
	@python -m venv bandit-env

bandit-test:
	@bandit -r app/ | tee -a reports/bandit.json

safety-check:
	@safety check --full-report --json | tee -a reports/safety.json 

hadolint:
	@hadolint -f json app/Dockerfile | tee -a reports/hadolint.json

flake:
	@flake8 --exit-zero --statistics app/ | tee -a reports/flake8.json
	@coverage coverage run --source=app -m flake8 --exit-zero --ignore=E121,E122,E126,E127,E128,E241,E251,E265,F401,F403,F405,W391 --max-line-length 150 app

pylama:
	@pylama app --format json | tee -a reports/pylama.json
	@coverage coverage run --source=app -m flake8 --exit-zero --ignore=E121,E122,E126,E127,E128,E241,E251,E265,F401,F403,F405,W391 --max-line-length 150 app

coverage:
	@coverage report --no-skip-covered -m app/*.py app/vapormap/*.py app/vapormap/settings/*.py app/api/*.py app/api/migrations/*.py
	@coverage html --no-skip-covered --show-contexts --ignore-errors app/*.py app/vapormap/*.py app/vapormap/settings/*.py app/api/*.py app/api/migrations/*.py
	@coverage xml --ignore-errors app/*.py app/vapormap/*.py app/vapormap/settings/*.py app/api/*.py app/api/migrations/*.py


compose:
	@docker-compose up --detach

clean:
	@docker rmi backend:latest frontend:latest
